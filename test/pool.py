from dataclasses import dataclass

@dataclass
class Pool:
    x: float
    y: float
    fee_percent: float

    def k(self) -> float:
        # Retourne la constante k = x * y
        return self.x * self.y

    def fee_factor(self) -> float:
        # Retourne le facteur de frais (100 - fee_percent) / 100
        return (100 - self.fee_percent) / 100

    def swap_x_for_y(self, x_in: float) -> float:
        # Calcule l'effet de frais
        x_in_effective = x_in * self.fee_factor()
        # Calcule le nouveau y après le swap
        new_x = self.x + x_in_effective
        new_y = self.k() / new_x
        y_out = self.y - new_y
        # Met à jour les réserves
        self.x += x_in
        self.y = new_y
        return y_out

    def swap_y_for_x(self, y_in: float) -> float:
        # Calcule l'effet de frais
        y_in_effective = y_in * self.fee_factor()
        # Calcule le nouveau x après le swap
        new_y = self.y + y_in_effective
        new_x = self.k() / new_y
        x_out = self.x - new_x
        # Met à jour les réserves
        self.x = new_x
        self.y += y_in
        return x_out

    def x_spot_price(self) -> float:
        return self.y / self.x

    def y_spot_price(self) -> float:
        return self.x / self.y
